
public class Loader {
    public static void main(String[] args) {

        int ketchupAmount = 10; // g
        int milkAmount = 300; // ml
        int tomatoAmount = 30; //
        int breadAmount = 50; // g
        int hamAmount = 200; // g
        int powderAmount = 20; // g
        int eggsCount = 6; // items
        int sugarAmount = 5; // g
        int oilAmount = 30; // ml
        int appleCount = 8;

        // apple - 3, powder - 2 g, milk - 500 ml,  oil - 20 ml
        if (appleCount >= 3 && powderAmount >= 2 && milkAmount >= 200 && oilAmount >= 20 ){
            System.out.println("Apple pie ready ");
        } else {
            System.out.println("Not enough ingredients to make apple pie ");
        }

        // milk - 100 ml, sugar - 1 g, powder - 10 g
        if (milkAmount >= 100 && powderAmount >= 2 && sugarAmount >= 1 ){
            System.out.println("bread ready");
        } else {
            System.out.println("Not enough ingredients to make bread  ");
        }

        //  tomatoAmount - 30, breadAmount - 45g, hamAmount - 189g, ketchupAmount - 8g
        if (tomatoAmount >= 28 && breadAmount >= 45 && hamAmount >= 189 && ketchupAmount >= 10){
            System.out.println("A sandwich ready ");
        } else {
            System.out.println("Not enough ingredients to make an sandwich ");
        }

        //powder - 400 g, sugar - 10 g, milk - 1 l, oil - 30 ml
        if ( powderAmount >=  400 && sugarAmount >=  10 && milkAmount >= 1000 && oilAmount >= 30) {
            System.out.println("Pancake ready ");
        } else {
            System.out.println("Not enough ingredients to make an Pancake ");
        }

        //milk - 300 ml, powder - 5 g, eggs - 5
        if (milkAmount >= 300 && powderAmount >= 5 && eggsCount >=  5) {
            System.out.println("Omelette ready");
        } else {
            System.out.println("Not enough ingredients to make an omelet ");
        }

        //apples - 3, milk - 100 ml, powder - 300 g, eggs - 4
        if ( appleCount >=  8 &&  milkAmount >=  100 && powderAmount >=  300 &&  eggsCount >=  4){
            System.out.println("Apple pie ready");
        } else {
            System.out.println("Not enough ingredients to make an apple pie");

        }
    }
}